import sys, urllib2
import datetime
import argparse, logging
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import colors as c
from scipy.interpolate import Rbf
from osgeo import gdal, osr
gdal.UseExceptions()
import xml.etree.cElementTree as ET
from xml.dom import minidom
import time

def writeToFile(grid2):
    np.savetxt('/var/www/html/pljusak/temperature/temp.txt', grid2,fmt='%.2f')

def timeStamped(fmt='%Y-%m-%d-%H-%M'):
        time = datetime.datetime.utcnow().strftime(fmt).split('-')
        minutes = time[4]
        newMinutes =int(minutes)//10*10
        time[4] = str(newMinutes).zfill(2)
        time = ('-').join(x for x in time)
        return time


def addElevationTIF(lon, lat, temperature, lapseRateMeter):
    temp = []
    for i in xrange(len(lon)):
        elev = getElevation(lon[i], lat[i])
        if (elev > 0):
            temp.append (elev*lapseRateMeter+float(temperature[i]))
        else:
            temp.append (None)
    return temp

def getElevation(x,y):
    x = round((float(x)-lonMin)/dx,0)
    y = round((float(y)-latMin)/dy,0)

    if y == len(elevationData):
        y -= 1
    if x == len(elevationData[0]):
        x -= 1
    elevation = elevationData[y, x]
    return elevation

def getDataFromStations(xMin, yMin, xMax, yMax, srid):
    xmlFile = urllib2.urlopen('http://pljusak.com/spec/ninotemp.xml')
    xmldoc = minidom.parse(xmlFile )
    locations = xmldoc.getElementsByTagName('location')

    latitude = []
    longitude =  []
    temperature = []
    elevation = []
    temperature = []

    for location in locations:
        try:
            lat = float(location.attributes['lat'].value)
            lon = float(location.attributes['lon'].value)
            elev = float(location.attributes['asl'].value)
            temp = float(location.attributes['temperature'].value)
            if (lat in latitude and lon in longitude):
                continue

        except ValueError:
            continue

        latitude.append(lat)
        longitude.append(lon)
        elevation.append(elev)
        temperature.append(temp)
    return longitude, latitude, elevation, temperature

def calculateLapseRateMeter(elevation, temperature):
    elevation = np.array(elevation, np.float)
    temperature = np.array(temperature, np.float)
    polynomialCoeffs = np.polyfit(elevation, temperature, 1,2)
    lapseRateMeter = polynomialCoeffs[0].round(6)
    return lapseRateMeter

def calculateTemp0m(temperature, elevation, lapseRateMeter):
    elevation = np.array(elevation, np.float)
    temperature = np.array(temperature, np.float)
    temp0m = (temperature[np.arange(len(temperature))]-
        (elevation[np.arange(len(temperature))]*lapseRateMeter))
    return temp0m

def createImage(longitude, latitude, temperature):
    fig = plt.figure()
    fig.set_size_inches(1,0.5777777777777)
    ax = fig.add_subplot(1,1,1)
    cMap = plt.cm.jet
    bounds = np.linspace(-25,35,61)
    norm = c.BoundaryNorm(bounds, cMap.N)
    plt.pcolormesh(longitude, latitude, temperature ,cmap=cMap, norm = norm)
    plt.axis([float(args.xMin), float(args.xMax), float(args.yMin), float(args.yMax)])
    plt.axis('off')
    extent = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
    plt.savefig("/var/www/html/pljusak/temperature/"+"pljusak.png",bbox_inches=extent,transparent=True,dpi=2000)
    plt.savefig("/var/www/html/pljusak/temperature/arhiva/"+timeStamped()+"-pljusak.png",bbox_inches=extent,transparent=True,dpi=2000)
    writeToFile(temperature[::-1])

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = 'Interpolation')
    parser.add_argument('--x_min', dest='xMin', default='12')
    parser.add_argument('--y_min', dest='yMin', default='42')
    parser.add_argument('--x_max', dest='xMax', default='21')
    parser.add_argument('--y_max', dest='yMax', default='47')
    parser.add_argument('--srid', dest='srid', default='4326')

    args = parser.parse_args()

    longitude, latitude, elevation, temperature = getDataFromStations(args.xMin,
     args.yMin, args.xMax, args.yMax, args.srid)
    lapseRateMeter = calculateLapseRateMeter(elevation, temperature)
    print lapseRateMeter
    temp0m = calculateTemp0m(temperature, elevation, lapseRateMeter)

    longitude = np.array(longitude, np.float)
    latitude = np.array(latitude, np.float)
    temp0m = np.array(temp0m, np.float)

    ds = gdal.Open('/var/www/interpolation/pljusakElev500m.tif')
    elevationData = ds.ReadAsArray()
    numRows, numCols = elevationData.shape
    lonMin, dx, dxdy, latMax, dydx, dy = ds.GetGeoTransform()
    ds = None

    lonMax = lonMin + dx * numCols
    latMin = latMax + dy * numRows

    chunkSize = 1200
    gridNumCols = 1200
    gridNumRows = 1200

    xi = np.linspace(float(args.xMin), float(args.xMax), gridNumCols)
    yi = np.linspace(float(args.yMin), float(args.yMax), gridNumRows)
    XI, YI = np.meshgrid(xi, yi)
    rbf = Rbf(longitude, latitude, temp0m, function = 'linear')

    grid = []
    start_time = time.time()
    chunksNum = len(xi)/chunkSize
    for i in xrange(chunkSize):
        start = chunksNum * i
        end = chunksNum * (i + 1)
        chunkX = XI[start:end]
        chunkY = YI[start:end]
        ZI = rbf(chunkX, chunkY)

        result = addElevationTIF(chunkX.flatten(), chunkY.flatten(),
            ZI.flatten(), lapseRateMeter)
        grid.extend(result)
    print("--- %s seconds ---" % (time.time() - start_time))
    grid = np.array(grid, np.float)
    grid = grid.reshape((gridNumCols, gridNumRows))
    print grid.shape
    tempMasked = np.ma.masked_where(np.isnan(grid), grid)
    createImage(XI, YI, tempMasked)



