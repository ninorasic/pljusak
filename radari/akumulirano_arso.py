import os

#!/usr/bin/env python
import os
import urllib
import argparse
from cStringIO import StringIO
from PIL import Image

class Radar(object):
    transparentWhite = (0,0,0,0)

    def downloadImage(self):
        file = StringIO(urllib.urlopen(self.url).read())
        image = Image.open(file)
        return image

    def saveRadarImage(self, image, filename, sum):
        rgbIm = image.convert('RGBA')
        pixels = rgbIm.load()
        if self.colorBar is None:
            self.colorBar = self.getColorBar(pixels)

        for y in xrange(self.yPixelBorders[0],self.yPixelBorders[1]):
            for x in xrange(self.xPixelBorders[0],self.xPixelBorders[1]):
                if pixels[x, y] in self.borderColors:
                    pixels[x, y] = pixels[x,y-1]
                if pixels[x,y] not in self.colorBar:
                    pixels[x, y] = self.transparentWhite
                    continue
                if self.colorBar.index(pixels[x,y]) < 4:
                    sum[x-10][y-49] += 2./6
                elif self.colorBar.index(pixels[x,y]) < 7:
                    sum[x-10][y-49] += 4./6
                elif pixels[x,y] == self.colorBar[7]:
                    sum[x-10][y-49] += 7./6
                elif pixels[x,y] == self.colorBar[8]:
                    sum[x-10][y-49] += 12./6
                elif pixels[x,y] == self.colorBar[9]:
                    sum[x-10][y-49] += 22./6
                elif pixels[x,y] == self.colorBar[10]:
                    sum[x-10][y-49] += 40./6
                elif pixels[x,y] == self.colorBar[11]:
                    sum[x-10][y-49] += 60./6
                elif pixels[x,y] == self.colorBar[12]:
                    sum[x-10][y-49] += 95./6
                elif pixels[x,y] == self.colorBar[13]:
                    sum[x-10][y-49] += 120./6
                elif pixels[x,y] == self.colorBar[14]:
                    sum[x-10][y-49] += 160./6


        width, height = rgbIm.size
        rgbIm = rgbIm.crop((self.leftCropOffset,self.topCropOffset,
            width + self.rightCropOffset,height + self.bottomCropOffset))
        #rgbIm.save('edit/'+filename, "PNG")


class ArsoRadar(Radar):
    def __init__(self):
        self.url = "http://www.arso.gov.si/vreme/napovedi%20in%20podatki/radar.gif"
        self.name = 'arso'
        self.borderColors = [(96, 96, 96, 255)]
        self.xPixelBorders = [10, 811]
        self.yPixelBorders = [49, 650]
        self.threshold = 3
        self.leftCropOffset = 10
        self.rightCropOffset = -10
        self.topCropOffset = 49
        self.bottomCropOffset = -10
        self.projection = 'EPSG:4326'
        self.upperLeft = [12.168894, 47.40554]
        self.bottomRight = [17.345599, 44.726983]
        self.activeMinutes = -10
        self.colorBar = None

    def getColorBar(self, pixels, rgbIm=None ):
        colorBar =  []
        for x in xrange(15):
            colorBar.append(pixels[610+(x*14),33])
        return colorBar
if __name__ == '__main__':

    parser = argparse.ArgumentParser(description ='Fetch radar image')
    parser.add_argument('--radar', dest='radar', choices=('arso'), default='arso')
    parser.add_argument('--fetch_all', dest='fetchAll', default=False)
    args = parser.parse_args()

    radars = dict(arso=ArsoRadar)
    sum = [[0]*601 for i in range(801)]
    max = 0
    def saveRadarImage(name, radars):
        radar = radars[name]()
        image = radar.downloadImage()
        radar.saveRadarImage(image)

    def sorted_ls(path):
        mtime = lambda f: os.stat(os.path.join(path, f)).st_mtime
        return list(sorted(os.listdir(path), key=mtime))

    radar = ArsoRadar()
    folder = '/var/www/html/pljusak/radari/arso'
    counter = 0
    for filename in reversed(sorted_ls(folder)):
        if '.py' in filename or '.php' in filename or 'edit' in filename or 'arhiva' in filename or 'dhmz' in filename or 'akum' in filename:
            continue

        if counter == 6*24:
            break
        print filename
        counter += 1
        try:
            image = Image.open(folder+'/'+filename)
            radar.saveRadarImage(image, filename, sum)
        except:
            pass
       # print folder+'/'+filename
    img = Image.new('RGBA',(801, 601))
    pixels = img.load()


    c1 = (255,255,255)
    c2 = (214,222,255)
    c3 = (181,197,255)
    c4 = (140,181,255)
    c5 = (99,115,247)
    c6 = (0,99,255)
    c7 = (0,148,148)
    c8 = (0,197,49)
    c9 = (99,255,0)
    c10 = (148,255,0)
    c11 = (197,255,49)
    c12 = (255,255,0)
    c13 = (255,156,0)
    c14 = (255,123,0)
    c15 = (255,25,0)
    c16 = (189,0,0)


    max = 0
    maxx = 0
    maxy = 0
    for x in xrange(801):
        for y in xrange(601):
            if sum[x][y] > 0:
                pixels[x,y] = c1
            if sum[x][y] > 5:
                pixels[x,y] = c2
            if sum[x][y] > 10:
                pixels[x,y] = c3
            if sum[x][y] > 20:
                pixels[x,y] = c4
            if sum[x][y] > 40:
                pixels[x,y] = c5
            if sum[x][y] > 70:
                pixels[x,y] = c6
            if sum[x][y] > 100:
                pixels[x,y] = c7
            if sum[x][y] > 150:
                pixels[x,y] = c8
            if sum[x][y] > 200:
                pixels[x,y] = c9
            if sum[x][y] > 250:
                pixels[x,y] = c10
            if sum[x][y] > 300:
                pixels[x,y] = c11
            if sum[x][y] > 400:
                pixels[x,y] = c12
            if sum[x][y] > 500:
                pixels[x,y] = c13
            if sum[x][y] > 600:
                pixels[x,y] = c14
            if sum[x][y] > 700:
                pixels[x,y] = c15
            if sum[x][y] > 800:
                pixels[x,y] = c16
            if sum[x][y] > max:
                max = sum[x][y]
                maxx = x
                maxy = y

    img.save('/var/www/html/pljusak/radari/arso_24h22.png','PNG')
    print "max je: " + str(max) +" x: " + str(maxx) + " y: " + str(maxy)
