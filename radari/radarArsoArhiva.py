import urllib, cStringIO
import datetime
import os


def timeStamped(fmt='%Y/%m/%d/%Y-%m-%d-%H-%M'):
        time = (datetime.datetime.utcnow() - datetime.timedelta(minutes=10)).strftime(fmt).split('-')
        minutes = time[4]
        newMinutes =int(minutes)//10*10
        time[4] = str(newMinutes).zfill(2)
        time = ('-').join(x for x in time)
        return time

   
url = "http://www.arso.gov.si/vreme/napovedi%20in%20podatki/radar.gif"
name = "arso"
folder_path = "/var/www/html/pljusak/radari/"
file_name = os.path.join(folder_path, name, timeStamped() + "-arso.gif")

if not os.path.exists(os.path.dirname(file_name)):
        os.makedirs(os.path.dirname(file_name))

urllib.urlretrieve(url, file_name)
