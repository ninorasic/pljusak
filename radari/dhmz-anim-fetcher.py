import requests as r
import shutil
import os
from dateutil.parser import parse as parsedate

URL = "https://vrijeme.hr/kompozit-anim.gif"
FILENAME = "dhmz_anim/{year:04}/{month:02}/{day:02}/dmhz_anim_{year:04}_{month:02}_{day:02}_{hour:02}_{minute:02}.gif"

response = r.get(URL, verify=False, stream=True, timeout=60)

last_modified = parsedate(response.headers["Last-Modified"])

filename = FILENAME.format(year=last_modified.year, month=last_modified.month, day=last_modified.day, 
	hour=last_modified.hour, minute=last_modified.minute)

if not os.path.exists(os.path.dirname(filename)):
	os.makedirs(os.path.dirname(filename))

if not os.path.exists(filename):
	with open(filename, "wb") as f:
		shutil.copyfileobj(response.raw, f)


