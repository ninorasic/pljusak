# -*- coding: utf-8 -*-

import glob
import os
import datetime as dt
import shutil

#archive_path = "/var/www/html/pljusak/radari/arso"
archive_path = "/mnt/volume_fra1_01/archive/radar/arso/old_files/arso"
new_archive_path = "/mnt/volume_fra1_01/archive/radar/arso"
date_pattern = "%Y-%m-%d-%H-%M-arso.gif"
new_date_pattern = '%Y/%m/%d/%Y-%m-%d-%H-%M-arso.gif'

files = glob.glob(os.path.join(archive_path, "*gif"))

for f in files:
    print f
    base_name = os.path.basename(f)
    dir_name = os.path.dirname(f)
    date = dt.datetime.strptime(base_name, date_pattern)
    date -= dt.timedelta(minutes=10)

    new_file_path = os.path.join(new_archive_path, date.strftime(new_date_pattern))

    new_dir_name = os.path.dirname(new_file_path)

    if not os.path.exists(new_dir_name):
        os.makedirs(new_dir_name)

    shutil.copy(f, new_file_path)






