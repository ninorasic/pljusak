# -*- coding: utf-8 -*-

import asyncio
import concurrent.futures
#import requests
import time
from itertools import cycle

import numpy as np
from glob import glob
import datetime as dt
import os

start_date = dt.datetime(2019, 2, 1)
end_date = dt.datetime(2019, 2, 5)
new_path = "/mnt/volume_fra1_01/archive/radar/arso"

date = start_date
dates = []

while date < end_date:
    dates.append(date)
    date += dt.timedelta(days=1)

pool = cycle(dates)

def process(date):
    pattern = os.path.join(new_path, date.strftime("%Y/%m/%d/%Y-%m-%d*_hourly.npz"))
    print (pattern)
    files = glob(pattern)
    print (files)

    s = None
    if len(files) == 0:
        return False
    for f in files:
        t = np.load(f)
        arr = t["arr"]

        arr[np.isnan(arr)] = 0

        if s is None:
            s = arr
        else:
            s += arr
    print (date)
    file_name = os.path.join(new_path, date.strftime("%Y/%m/%d/%Y-%m-%d_daily"))
    dir_name = os.path.dirname(file_name)

    if not os.path.exists(dir_name):
        os.makedirs(dir_name)

    np.savez_compressed(file_name, arr=s)

    return True

async def main():

    with concurrent.futures.ProcessPoolExecutor(max_workers=20) as executor:

        loop = asyncio.get_event_loop()
        futures = [
            loop.run_in_executor(
                executor,
                process,
                next(pool)
            )
            for i in range(len(dates))
        ]
        # for response in await asyncio.gather(*futures):
        #     pass


loop = asyncio.get_event_loop()
start =  time.time()
loop.run_until_complete(main())
print (time.time() - start )
