<?php
header("Content-type: image/png");
header("Cache-Control: no-cache"); 
header("Pragma: no-cache"); 

$imgName="http://www.arso.gov.si/vreme/napovedi%20in%20podatki/radar.gif";
$src = imagecreatefromgif($imgName);
$dest = imagecreatetruecolor(801, 601);
$color = array();
$snow1 = imagecolorallocate($src, 255,194,255);
$snow2 = imagecolorallocate($src, 253, 142, 227);
$snow3 = imagecolorallocate($src, 249, 105, 191);
$snow4 = imagecolorallocate($src, 239, 76, 172);

$rain1 = imagecolorallocate($src, 0,200,254);
$rain2 = imagecolorallocate($src, 66,235,66);
$rain3 = imagecolorallocate($src, 254,195,0);
$rain4 = imagecolorallocate($src, 181,3,3);


$white = imagecolorallocate($src, 255, 255, 255);


$arsoE = 12.168894;
$arsoW = 17.345599;
$arsoN = 47.40554;
$arsoS = 44.726983;

$resXarso = 801;
$resYarso = 601;

$xPx = ($arsoW - $arsoE)/$resXarso;
$yPx = ($arsoN - $arsoS)/$resYarso;

$lastLine = -1;
for ($c=0;$c<15;$c++){
	$color[] = imagecolorat($src, 610+($c*14), 33);
}
$granice = imagecolorexact($src, 96, 96, 96);

for ($y = 44; $y < 655; $y++) {
	for ($x = 5; $x < 816; $x++) {
		$rgb = imagecolorat($src, $x, $y);
		$key = array_search($rgb, $color);
		if ($rgb == $granice) {
			$rgb1 = imagecolorat($src, $x, $y-1);
			imagesetpixel($src, $x,$y, $rgb1 );
		}
		else if  ($key == false) {
			imagecolorset($src, $rgb, 255, 255, 255);
		} 
		else 
		{
			$temp = returnTemp($arsoE + ($x-10)*$xPx, $arsoN - ($y-47)*$yPx);
			if ($temp < 1) imagesetpixel($src, $x,$y, $snow4 );
			else if ($temp < 1.3) imagesetpixel($src, $x,$y, $snow3 );
			else if ($temp < 1.7) imagesetpixel($src, $x,$y, $snow2 );
			else if ($temp <= 2.5) imagesetpixel($src, $x,$y, $snow1 );
			else if ($key <4) imagesetpixel($src, $x,$y, $rain1); 
			else if ($key <7) imagesetpixel($src, $x,$y, $rain2); 
			else if ($key <11) imagesetpixel($src, $x,$y, $rain3); 
			else if ($key <16) imagesetpixel($src, $x,$y, $rain4); 
			
		}

	}
}

imagecopy($dest, $src, -10, -49	, 0, 0, 821, 660);
$white = imagecolorallocate($dest, 255, 255, 255);
imagecolortransparent($dest, $white);

imagepng($dest, "/var/www/html/pljusak/radari/arso_SR.png");
imagedestroy($src);
imagedestroy($dest);

function returnTemp ($postX,$postY) {
$xcodmin = 8.5;
$ycodmin = 40.4;
$xcodmax = 23.7;
$ycodmax = 48.3;

$resX = 1200;
$resY = 1200;

$x = ($xcodmax - $xcodmin)/$resX;
$y = ($ycodmax - $ycodmin)/$resY;

global $lastLine, $lLine;
if ($postY  >= $ycodmin and  $postY  <= $ycodmax and $postX >= $xcodmin and  $postX <= $xcodmax) {
$myLine = round(($ycodmax-$postY)/$y); 
if ($lastLine != $myLine) {
$file = new SplFileObject('/var/www/html/pljusak/temperature/temp.txt');
$file->seek($myLine);
$line = split(" ",$file->current());
$lastLine = $myLine;
$lLine = $line;
}
else {
$line = $lLine;
}
$temp = $line[round(($postX-$xcodmin)/$x)]; 
if ($temp == 'nan') return 100;
else return $temp;
}
else return 100; 
}
?>
