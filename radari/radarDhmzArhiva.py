import urllib, cStringIO
import datetime


def timeStamped(fmt='%Y-%m-%d-%H-%M'):
        time = datetime.datetime.utcnow().strftime(fmt).split('-')
        minutes = time[4]
        newMinutes =int(minutes)//15*15
        time[4] = str(newMinutes).zfill(2)
        time = ('-').join(x for x in time)
        return time


url = "http://vrijeme.hr/kradar.gif"
name = 'dhmz'
urllib.urlretrieve(url, '/var/www/html/pljusak/radari/' + name + '/'+ timeStamped() + '-dhmz.gif')
