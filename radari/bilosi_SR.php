<?php
//citanje i kreiranje slike sa linka
$imgName="http://vrijeme.hr/kradar.gif";
$src = imagecreatefromgif($imgName);
$snow1 = imagecolorallocate($src, 255,194,255);
$snow2 = imagecolorallocate($src, 253, 142, 227);
$snow3 = imagecolorallocate($src, 249, 105, 191);
$snow4 = imagecolorallocate($src, 239, 76, 172);

$rain1 = imagecolorallocate($src, 0,200,254);
$rain2 = imagecolorallocate($src, 66,235,66);
$rain3 = imagecolorallocate($src, 254,195,0);
$rain4 = imagecolorallocate($src, 181,3,3);

//kreiranje nove slike zbog cropa
$dest = imagecreatetruecolor(478, 474);   //478 474

$bilosiE = 14.05233277225926;
$bilosiW = 20.92840929588522;
$bilosiN = 48.09306888145721;
$bilosiS = 43.33537006612862;

$resXbilosi = 478;
$resYbilosi = 474;

$xPx = ($bilosiW - $bilosiE)/$resXbilosi;
$yPx = ($bilosiN - $bilosiS)/$resYbilosi;
$white = imagecolorallocate($src, 255, 255, 255);
$lastLine = -1;

//"citanje" skale boja koje oznacavaju oborine s radarske slike
for ($c=0;$c<5;$c++){
	$color[] = imagecolorat($src, 555, 240+($c*50));
}
$c1 = imagecolorexact($src, 0, 0, 0);
$c2 = imagecolorexact($src, 0, 89, 129);
$granice[] = [$c1, $c2];
//svaki pixel koji nije oborina oboji u bijelo
for ($y = 0; $y < 480; $y++) {
  for ($x = 0; $x < 640; $x++) {
       $rgb = imagecolorat($src, $x, $y);
	   $key = array_search($rgb, $color);
	   if (in_array($rgb, $granice) and $y >0 ) {
			$rgb1 = imagecolorat($src, $x, $y-1);
	       imagesetpixel($src, $x,$y, $rgb1 );
		}
		else if  ($key == false) {
            //echo $rgb;
			imagecolorset($src, $rgb, 255, 255, 255);
		} 
		else 
		{
			$temp = returnTemp($bilosiE + ($x-1)*$xPx, $bilosiN - ($y-6)*$yPx);
			if ($temp < 1) imagesetpixel($src, $x,$y, $snow4 );
			else if ($temp < 1.3) imagesetpixel($src, $x,$y, $snow3 );
			else if ($temp < 1.7) imagesetpixel($src, $x,$y, $snow2 );
			else if ($temp <= 2.5) imagesetpixel($src, $x,$y, $snow1 );
			else if ($key == 0) imagesetpixel($src, $x,$y, $rain4); //0,200,254
			else if ($key == 1) imagesetpixel($src, $x,$y, $rain3); //66,235,66
			else if ($key == 2) imagesetpixel($src, $x,$y, $rain2); //254,195,0
		    else if ($key == 3) imagesetpixel($src, $x,$y, $rain1); //181,3,3
			else if ($key == 4) imagesetpixel($src, $x,$y, $white);
			
		}
   }
}

//crop slike
imagecopy($dest, $src, -1, -6, 0, 0, 640, 480);

//di god je bijela boja stavi da je prozirno
$white = imagecolorallocate($dest, 255, 255, 255);
imagecolortransparent($dest, $white);

//spremanje slike
imagegif($dest, "/var/www/html/pljusak/radari/bilosi_SR.gif");
imagedestroy($src);
imagedestroy($dest);

function returnTemp ($postX,$postY) {
$xcodmin = 8.5;
$ycodmin = 40.4;
$xcodmax = 23.7;
$ycodmax = 48.3;

$resX = 1200;
$resY = 1200;

$x = ($xcodmax - $xcodmin)/$resX;
$y = ($ycodmax - $ycodmin)/$resY;

global $lastLine, $lLine;
if ($postY  >= $ycodmin and  $postY  <= $ycodmax and $postX >= $xcodmin and  $postX <= $xcodmax) {
$myLine = round(($ycodmax-$postY)/$y); 
if ($lastLine != $myLine) {
$file = new SplFileObject('/var/www/html/pljusak/temperature/temp.txt');
$file->seek($myLine);
$line = split(" ",$file->current());
$lastLine = $myLine;
$lLine = $line;
}
else {
$line = $lLine;
}
$temp = $line[round(($postX-$xcodmin)/$x)]; 
if ($temp == 'nan') return 100;
else return $temp;
}
else return 100; 
}

?>
